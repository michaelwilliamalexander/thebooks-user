package id.ac.ukdw.thebooks_user.feature.add;

import id.ac.ukdw.thebooks_user.config.ObjectMapping;
import id.ac.ukdw.thebooks_user.controller.UserController;
import id.ac.ukdw.thebooks_user.dto.request.SignUpRequest;
import id.ac.ukdw.thebooks_user.exception.BadRequestException;
import id.ac.ukdw.thebooks_user.model.TableUser;
import id.ac.ukdw.thebooks_user.model.User;
import id.ac.ukdw.thebooks_user.service.UserService;
import io.cucumber.java.Before;
import io.cucumber.java.en.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class AddUserFailTest {

    @InjectMocks
    private UserController controller;

    @Mock
    private UserService service;

    private MockMvc mockMvc;

    private List<User> user;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        user = new ArrayList<>();
    }

    @Given("^List data-data user$")
    public void data(List<TableUser> data) {
        for (TableUser item:data){
            user.add(new User(
                    item.getEmail(),
                    item.getNamaUser(),
                    item.getNomorTelepon(),
                    item.getTanggalLahir(),
                    item.getFotoUser().getBytes()));
        }
    }

    @When("^user akan menambahkan user dengan data (.*) , (.*) , (.*) , (.*)$")
    public void input(final String email,
                      final String nama,
                      final String tanggal,
                      final String nomor) {
        for (User item:user){
            if (email.equals(item.getEmail()) || email.isEmpty() || nama.isEmpty()){
                when(service.addUser(email,nama,tanggal,nomor)).thenThrow(new BadRequestException());
                break;
            }
        }

    }

    @Then("^user memeperoleh pesan Bad Request dari data request (.*) , (.*) , (.*) , (.*)$")
    public void output(final String email,
                       final String nama,
                       final String tanggal,
                       final String nomor) throws Exception{

        mockMvc.perform(MockMvcRequestBuilders.post("/auth/signup")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ObjectMapping.asJsonString(new SignUpRequest(
                        email,
                        nama,
                        tanggal,
                        nomor))))
                .andExpect(status()
                        .isBadRequest()).andDo(print());

        verify(service).addUser(
                email,
                nama,
                tanggal,
                nomor);
    }
}
