package id.ac.ukdw.thebooks_user.feature.update;

import id.ac.ukdw.thebooks_user.config.ObjectMapping;
import id.ac.ukdw.thebooks_user.controller.UserController;
import id.ac.ukdw.thebooks_user.dto.request.UpdateUserRequest;
import id.ac.ukdw.thebooks_user.exception.BadRequestException;
import id.ac.ukdw.thebooks_user.model.TableUser;
import id.ac.ukdw.thebooks_user.model.User;
import id.ac.ukdw.thebooks_user.service.UserService;
import io.cucumber.java.Before;
import io.cucumber.java.en.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class UpdateUserFailTest {

    @InjectMocks
    private UserController controller;

    @Mock
    private UserService service;

    private MockMvc mockMvc;

    private List<User> user;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        user = new ArrayList<>();
    }

    @Given("^List-list data User$")
    public void data(List<TableUser> data) {
        for (TableUser item : data) {
            if (item.getFotoUser() == null) {
                user.add(new User(
                        item.getEmail(),
                        item.getNamaUser(),
                        item.getNomorTelepon(),
                        item.getTanggalLahir()));
            } else {
                user.add(new User(
                        item.getEmail(),
                        item.getNamaUser(),
                        item.getNomorTelepon(),
                        item.getTanggalLahir(),
                        item.getFotoUser().getBytes()));
            }
        }
    }

    @When("^user dengan email (.*) memperbarui user dengan request (.*) , (.*) , (.*) , (.*)$")
    public void input(final String email,
                      final String namaUser,
                      final String tanggalLahir,
                      final String nomorTelepon,
                      final String fotoUser) throws IOException {
        MultipartFile foto = new MockMultipartFile(
                "images",
                fotoUser,
                MediaType.MULTIPART_FORM_DATA_VALUE,
                fotoUser.getBytes()
        );

        when(service.updateUser(email, namaUser, nomorTelepon, tanggalLahir, foto.getBytes()))
                .thenThrow(new BadRequestException());
    }

    @Then("^user mendapatkan Bad Request dari update user dengan request (.*) , (.*) , (.*) , (.*) , (.*)$")
    public void output(final String email,
                       final String namaUser,
                       final String tanggalLahir,
                       final String nomorTelepon,
                       final String fotoUser) throws Exception {
        MockMultipartFile foto = new MockMultipartFile(
                "images",
                fotoUser,
                MediaType.MULTIPART_FORM_DATA_VALUE,
                fotoUser.getBytes()
        );

        UpdateUserRequest request = new UpdateUserRequest(
                email,
                namaUser,
                tanggalLahir,
                nomorTelepon);

        MockMultipartFile data = new MockMultipartFile(
                "data",
                "data",
                MediaType.APPLICATION_JSON_VALUE,
                ObjectMapping.asJsonString(request).getBytes()
        );

        mockMvc.perform(MockMvcRequestBuilders.multipart("/auth/profile")
                .file(foto)
                .file(data))
                .andDo(print())
                .andExpect(status().isBadRequest());

        verify(service).updateUser(email,namaUser,nomorTelepon,tanggalLahir,foto.getBytes());

    }
}
