package id.ac.ukdw.thebooks_user.feature.get;

import com.fasterxml.jackson.core.JsonProcessingException;
import id.ac.ukdw.thebooks_user.config.ObjectMapping;
import id.ac.ukdw.thebooks_user.controller.UserController;
import id.ac.ukdw.thebooks_user.dto.request.SignInRequest;
import id.ac.ukdw.thebooks_user.model.TableUser;
import id.ac.ukdw.thebooks_user.model.User;
import id.ac.ukdw.thebooks_user.service.UserService;
import io.cucumber.java.Before;
import io.cucumber.java.en.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class SignInTest {

    @InjectMocks
    private UserController controller;

    @Mock
    private UserService service;

    private MockMvc mockMvc;

    private List<User> user;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        user = new ArrayList<>();
    }

    @Given("^Terlist data user$")
    public void data(List<TableUser> data ) {
        for (TableUser item:data){
            if (item.getFotoUser() == null){
                user.add(new User(
                        item.getEmail(),
                        item.getNamaUser(),
                        item.getNomorTelepon(),
                        item.getTanggalLahir()));
            }else{
                user.add(new User(
                        item.getEmail(),
                        item.getNamaUser(),
                        item.getNomorTelepon(),
                        item.getTanggalLahir(),
                        item.getFotoUser().getBytes()));
            }
        }
    }

    @When("^user dengan email (.*) ingin login ke sistem$")
    public void input(final String email) {
        for (User item: user){
            if (item.getEmail().equals(email)){
                when(service.signIn(email)).thenReturn(true);
                break;
            }else{
                when(service.signIn(email)).thenReturn(false);
            }
        }
    }
    @Then("^user mendapatkan status akun (.*) dengan request email (.*)$")
    public void output(final boolean statusAkun, final String email) throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.post("/auth/signin")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ObjectMapping.asJsonString(new SignInRequest(email))))
                .andExpect(jsonPath("$.data",is(statusAkun)))
                .andDo(print());

        verify(service).signIn(email);
    }
}
