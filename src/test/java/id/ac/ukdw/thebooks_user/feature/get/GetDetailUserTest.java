package id.ac.ukdw.thebooks_user.feature.get;

import id.ac.ukdw.thebooks_user.config.ObjectMapping;
import id.ac.ukdw.thebooks_user.controller.UserController;
import id.ac.ukdw.thebooks_user.dto.UserDTO;
import id.ac.ukdw.thebooks_user.dto.response.ResponseWrapper;
import id.ac.ukdw.thebooks_user.model.TableUser;
import id.ac.ukdw.thebooks_user.model.User;
import id.ac.ukdw.thebooks_user.service.UserService;
import io.cucumber.java.Before;
import io.cucumber.java.en.*;
import org.junit.Assert;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class GetDetailUserTest {

    @InjectMocks
    private UserController controller;

    @Mock
    private UserService service;


    @Autowired
    private ModelMapper mapper;

    private MockMvc mockMvc;

    private List<User> user;

    private UserDTO result;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        user = new ArrayList<>();
    }

    @Given("Terdapat data user account")
    public void data(List<TableUser> data) throws IOException {
        for (TableUser item:data){
            if (item.getFotoUser() == null){
                user.add(new User(
                        item.getEmail(),
                        item.getNamaUser(),
                        item.getNomorTelepon(),
                        item.getTanggalLahir()));
            }else{
                MockMultipartFile foto = new MockMultipartFile(
                        "images",
                        item.getFotoUser(),
                        MediaType.IMAGE_JPEG_VALUE,
                        item.getFotoUser().getBytes()
                );

                user.add(new User(
                        item.getEmail(),
                        item.getNamaUser(),
                        item.getNomorTelepon(),
                        item.getTanggalLahir(),
                        foto.getBytes()));
            }
        }
    }

    @When("^user ingin mendapatkan data user dengan email (.*)$")
    public void input(final String email) {
        for (User item: user){
            if(item.getEmail().equals(email)){
                result = mapper.map(item, UserDTO.class);
                when(service.getDetailUser(email)).thenReturn(result);
            }
        }
    }

    @Then("^user mendapatkan detail data dengan email (.*)$")
    public void output(final String email) throws Exception {

        final MvcResult test = mockMvc.perform(MockMvcRequestBuilders.get("/auth/profile")
                .contentType(MediaType.APPLICATION_JSON)
                .param("email",email))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.email",is(result.getEmail())))
                .andExpect(jsonPath("$.data.namaUser",is(result.getNamaUser())))
                .andExpect(jsonPath("$.data.nomorTelepon",is(result.getNomorTelepon())))
                .andExpect(jsonPath("$.data.tanggalLahir",is(result.getTanggalLahir())))
                .andExpect(content().bytes(ObjectMapping.asJsonString(new ResponseWrapper(result)).getBytes()))
                .andDo(print()).andReturn();

        verify(service).getDetailUser(email);


    }
}
