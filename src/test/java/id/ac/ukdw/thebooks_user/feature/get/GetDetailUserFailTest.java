package id.ac.ukdw.thebooks_user.feature.get;

import id.ac.ukdw.thebooks_user.controller.UserController;
import id.ac.ukdw.thebooks_user.dto.UserDTO;
import id.ac.ukdw.thebooks_user.exception.BadRequestException;
import id.ac.ukdw.thebooks_user.exception.NotFoundException;
import id.ac.ukdw.thebooks_user.model.TableUser;
import id.ac.ukdw.thebooks_user.model.User;
import id.ac.ukdw.thebooks_user.service.UserService;
import io.cucumber.java.Before;
import io.cucumber.java.en.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class GetDetailUserFailTest {

    @InjectMocks
    private UserController controller;

    @Mock
    private UserService service;

    @Autowired
    private ModelMapper mapper;

    private MockMvc mockMvc;

    private List<User> user;

    private UserDTO result;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        user = new ArrayList<>();
    }

    @Given("^Terdapat data-data user account$")
    public void data(List<TableUser> data) {
        for (TableUser item:data){
            if (item.getFotoUser() == null){
                user.add(new User(
                        item.getEmail(),
                        item.getNamaUser(),
                        item.getNomorTelepon(),
                        item.getTanggalLahir()));
            }else{

                user.add(new User(
                        item.getEmail(),
                        item.getNamaUser(),
                        item.getNomorTelepon(),
                        item.getTanggalLahir(),
                        item.getFotoUser().getBytes()));
            }
        }
    }

    @When("^user mendapatkan data user dengan email (.*)$")
    public void input(final String email) {
        when(service.getDetailUser(email)).thenThrow(new NotFoundException());
    }

    @Then("^user mendapatkan Not Found dari detail data dengan email (.*)$")
    public void output(final String email) throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.get("/auth/profile")
                .contentType(MediaType.APPLICATION_JSON)
                .param("email",email))
                .andExpect(status().isNotFound())
                .andDo(print());

        verify(service).getDetailUser(email);
    }
}
