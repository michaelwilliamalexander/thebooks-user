package id.ac.ukdw.thebooks_user.model;

import lombok.Data;

import javax.persistence.Column;

@Data
public class TableUser {

    private String email;

    private String namaUser;

    private String nomorTelepon;

    private String tanggalLahir;

    private String fotoUser;

}
