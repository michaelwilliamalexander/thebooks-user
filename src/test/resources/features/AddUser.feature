Feature: Menambahkan User Acoount

  Scenario Outline: Menambahkan data dan Berhasil
    Given List data user account
      |email                         |namaUser        |tanggalLahir |nomorTelepon |fotoUser  |
      |michael.william@ti.ukdw.ac.id |Michael William |1999-04-01   |081324101010 |image.png |
      |nathaniel.alvin@ti.ukdw.ac.id |Nathaniel Alvin |1999-08-01   |081324101010 |image.png |
    When user ingin menambahkan user dengan data <email> , <namaUser> , <tanggalLahir> , <nomorTelepon>
    Then user mendapatkan pesan <pesan> dari data request <email> , <namaUser> , <tanggalLahir> , <nomorTelepon>
    Examples:
      |email                              |namaUser             |tanggalLahir |nomorTelepon |pesan        |
      |ananda.kusumawardana@ti.ukdw.ac.id |Ananda Kusumawardana |1999-12-01   |081324101010 |Akun terbuat |

  Scenario Outline: Menambahkan data dan Gagal
    Given List data-data user
      |email                                |namaUser             |tanggalLahir |nomorTelepon |fotoUser  |
      |michael.william@ti.ukdw.ac.id        |Michael William      |1999-04-01   |081324101010 |image.png |
      |nathaniel.alvin@ti.ukdw.ac.id        |Nathaniel Alvin      |1999-08-01   |081324101010 |image.png |
      |ananda.kusumawardana@ti.ukdw.ac.id   |Ananda Kusumawardana |1999-12-01   |081324101010 |image.png |
    When user akan menambahkan user dengan data <email> , <namaUser> , <tanggalLahir> , <nomorTelepon>
    Then user memeperoleh pesan Bad Request dari data request <email> , <namaUser> , <tanggalLahir> , <nomorTelepon>
    Examples:
      |email                                |namaUser             |tanggalLahir       |nomorTelepon       |
      |ananda.kusumawardana@ti.ukdw.ac.id   |Ananda Kusumawardana |1999-12-01         |081324101010       |
      |ananda.kusumawardana@ti.ukdw.ac.id   |                     |1999-12-01         |081324101010       |
      |                                     |Ananda Kusumawardana |1999-12-01         |081324101010       |
      |                                     |                     |1999-12-01         |081324101010       |