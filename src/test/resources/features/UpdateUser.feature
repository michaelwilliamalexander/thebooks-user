Feature: Memperbarui User Acoount

  Scenario Outline: Memperbarui data User Account dan Berhasil
    Given Terdapat data list user
      |email                                |namaUser             |tanggalLahir       |nomorTelepon       |fotoUser         |
      |michael.william@ti.ukdw.ac.id        |Michael William      |1999-04-01          |081324101010       |imager.png       |
      |nathaniel.alvin@ti.ukdw.ac.id        |Nathaniel Alvin      |1999-08-01         |081324101010       |                 |
      |ananda.kusumawardana@ti.ukdw.ac.id   |Ananda Kusumawardana |1999-12-01         |081324101010       |images.png       |
    When user dengan email <email> memperbarui data <namaUser> , <tanggalLahir> , <nomorTelepon> , <fotoUser>
    Then user mendapatkan pesan <pesan> dari update user dengan request <email> , <namaUser> , <tanggalLahir> , <nomorTelepon> , <fotoUser>
    Examples:
    |email                                |namaUser             |tanggalLahir       |nomorTelepon       |fotoUser       |pesan           |
    |nathaniel.alvin@ti.ukdw.ac.id        |Alvin                |1999-08-01         |081324101010       |pokemon.png    |Update Berhasil |
    |michael.william@ti.ukdw.ac.id        |Michael              |1999-08-01         |081324101010       |               |Update Berhasil |

  Scenario Outline: Memperbarui data User Account dan Gagal
    Given List-list data User
      |email                                |namaUser             |tanggalLahir       |nomorTelepon       |fotoUser         |
      |michael.william@ti.ukdw.ac.id        |Michael William      |1999-04-01         |081324101010       |images.png       |
      |nathaniel.alvin@ti.ukdw.ac.id        |Nathaniel Alvin      |1999-08-01         |081324101010       |                 |
      |ananda.kusumawardana@ti.ukdw.ac.id   |Ananda Kusumawardana |1999-12-01         |081324101010       |images.png       |
    When user dengan email <email> memperbarui user dengan request <namaUser> , <tanggalLahir> , <nomorTelepon> , <fotoUser>
    Then user mendapatkan Bad Request dari update user dengan request <email> , <namaUser> , <tanggalLahir> , <nomorTelepon> , <fotoUser>
    Examples:
      |email                                |namaUser             |tanggalLahir       |nomorTelepon       |fotoUser       |
      |nathaniel.alvin@ti.ukdw.ac.id        |                     |1999-08-01         |081324101010       |pokemon.png    |
      |                                     |                     |1999-08-01         |081324101010       |pokemon.png    |

