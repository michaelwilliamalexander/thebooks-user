Feature: Mendapatkan data User

  Scenario Outline: Mendapatkan Detail Data User Account dan Ditemukan
    Given Terdapat data user account
      |email                                |namaUser             |tanggalLahir       |nomorTelepon       |fotoUser         |
      |michael.william@ti.ukdw.ac.id        |Michael William      |1999-04-01         |081324101010       |images.png       |
      |nathaniel.alvin@ti.ukdw.ac.id        |Nathaniel Alvin      |1999-08-01         |081324101010       |                 |
      |ananda.kusumawardana@ti.ukdw.ac.id   |Ananda Kusumawardana |1999-12-01         |081324101010       |images.png       |
    When user ingin mendapatkan data user dengan email <email>
    Then user mendapatkan detail data dengan email <email>
    Examples:
      |email                              |
      |nathaniel.alvin@ti.ukdw.ac.id      |
      |michael.william@ti.ukdw.ac.id      |
      |ananda.kusumawardana@ti.ukdw.ac.id |

  Scenario Outline: Mendapatkan Detail Data User Account dan Data Tidak Ditemukan
    Given Terdapat data-data user account
      |email                                |namaUser             |tanggalLahir       |nomorTelepon       |fotoUser         |
      |michael.william@ti.ukdw.ac.id        |Michael William      |1999-04-01         |081324101010       |images.png       |
      |nathaniel.alvin@ti.ukdw.ac.id        |Nathaniel Alvin      |1999-08-01         |081324101010       |                 |
      |ananda.kusumawardana@ti.ukdw.ac.id   |Ananda Kusumawardana |1999-12-01         |081324101010       |images.png       |
    When user mendapatkan data user dengan email <email>
    Then user mendapatkan Not Found dari detail data dengan email <email>
    Examples:
      |email                              |
      |ananda.kusumawardana@ti.ukdw.ac.id |

  Scenario Outline: Sign In akun
    Given Terlist data user
      |email                                |namaUser             |tanggalLahir       |nomorTelepon       |fotoUser         |
      |michael.william@ti.ukdw.ac.id        |Michael William      |1999-04-01         |081324101010       |images.png       |
      |nathaniel.alvin@ti.ukdw.ac.id        |Nathaniel Alvin      |1999-08-01         |081324101010       |                 |
    When user dengan email <email> ingin login ke sistem
    Then user mendapatkan status akun <status> dengan request email <email>
    Examples:
    |email                              |status     |
    |michael.william@ti.ukdw.ac.id      |true       |
    |ananda.kusumawardana@ti.ukdw.ac.id |false      |
