package id.ac.ukdw.thebooks_user.repository.dao;

import java.util.Optional;

public interface Dao <T,ID> {

    Optional<T> findById(ID email);

    boolean save(T t);

    boolean update(T t);

}
