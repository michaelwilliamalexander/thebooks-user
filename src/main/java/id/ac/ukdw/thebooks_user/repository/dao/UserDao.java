package id.ac.ukdw.thebooks_user.repository.dao;

import id.ac.ukdw.thebooks_user.model.User;

public interface UserDao extends Dao<User,String>{

}
