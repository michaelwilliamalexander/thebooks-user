package id.ac.ukdw.thebooks_user.repository;

import id.ac.ukdw.thebooks_user.model.User;
import id.ac.ukdw.thebooks_user.repository.dao.UserDao;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
@RequiredArgsConstructor
public class UserRepostiory implements UserDao {

    private final EntityManager entityManager;

    @Override
    public Optional<User> findById(String email) {
        String hql = "select usr from User usr where usr.email = :email";
        Query query = entityManager.createQuery(hql);
        query.setParameter("email", email);
        List<User> userList = query.getResultList();
        User user = null;
        for(User usr:userList){
            user = usr;
        }
        return Optional.ofNullable(user);
    }

    @Override
    public boolean save(User user) {
        String hql = "insert into user(email_user,nama_user,nomor_telepon,tanggal_lahir,foto_user) values(?,?,?,?,?)";
        Query query = entityManager.createNativeQuery(hql);
        query.setParameter(1, user.getEmail());
        query.setParameter(2, user.getNamaUser());
        query.setParameter(3, user.getNomorTelepon());
        query.setParameter(4, user.getTanggalLahir());
        query.setParameter(5,"");
        query.executeUpdate();
        return true;
    }

    @Override
    public boolean update(User user) {
        entityManager.merge(user);
        return true;
    }

}
