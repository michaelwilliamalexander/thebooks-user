package id.ac.ukdw.thebooks_user.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
@Data
@Table(name="user")
public class User {

    @Id @NotEmpty @NotEmpty
    @Column(name="email_user")
    private String email;

    @NotEmpty @NotNull
    @Column(name = "nama_user")
    private String namaUser;

    @Column(name = "nomor_telepon")
    private String nomorTelepon;

    @Column(name = "tanggal_lahir",columnDefinition = "date")
    private String tanggalLahir;

    @Lob
    @Column(name = "foto_user",columnDefinition = "longblob")
    private byte[] fotoUser;
    
    public User(){}
    
    public User(String email,
                String namaUser, 
                String nomorTelepon, 
                String tanggalLahir,
                byte[] fotoUser){
        this.email = email;
        this.namaUser = namaUser;
        this.nomorTelepon = nomorTelepon;
        this.tanggalLahir = tanggalLahir;
        this.fotoUser = fotoUser;
    }

    public User(String email,
                String namaUser,
                String nomorTelepon,
                String tanggalLahir){
        this.email = email;
        this.namaUser = namaUser;
        this.nomorTelepon = nomorTelepon;
        this.tanggalLahir = tanggalLahir;
    }

}