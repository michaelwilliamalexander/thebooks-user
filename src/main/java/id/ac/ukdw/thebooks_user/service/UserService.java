package id.ac.ukdw.thebooks_user.service;

import id.ac.ukdw.thebooks_user.dto.UserDTO;
import id.ac.ukdw.thebooks_user.exception.BadRequestException;
import id.ac.ukdw.thebooks_user.exception.NotFoundException;
import id.ac.ukdw.thebooks_user.model.User;
import id.ac.ukdw.thebooks_user.repository.dao.UserDao;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static org.valid4j.Validation.validate;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserDao userRepository;

    private final ModelMapper mapper;

    public boolean signIn(String email){
        Optional<User> user = userRepository.findById(email);
        return user.isPresent();
    }

    public UserDTO getDetailUser(String email){
        Optional<User> find = userRepository.findById(email);
        if(find.isPresent()){
            return mapper.map(find.get(),UserDTO.class);
        }
        throw new NotFoundException();
    }

    public String addUser(String email,
                          String namaUser,
                          String tanggalLahir,
                          String nomorTelepon){
        validate(namaUser!=null, new BadRequestException());
        validate(!namaUser.isEmpty(), new BadRequestException());
        validate(nomorTelepon!=null, new BadRequestException());
        validate(!nomorTelepon.isEmpty(), new BadRequestException());

        Optional<User> findData =  userRepository.findById(email);
        if (findData.isEmpty()){
            User user = new User(email, namaUser, nomorTelepon, tanggalLahir);
           if (userRepository.save(user)){
               return "Akun Terbuat";
           }
        }
        throw new BadRequestException();
    }

    @SneakyThrows
    public String updateUser(String email,
                            String namaUser,
                            String nomorTelepon,
                            String tanggalLahir,
                            byte[] fotoUser){
        validate(namaUser!=null, new BadRequestException());
        validate(!namaUser.isEmpty(), new BadRequestException());
        validate(nomorTelepon!=null, new BadRequestException());
        validate(!nomorTelepon.isEmpty(), new BadRequestException());

        Optional<User> find = userRepository.findById(email);
        if(find.isPresent()){
            User user = new User(email,namaUser,nomorTelepon,tanggalLahir,fotoUser);
            if(userRepository.update(user)) {
                return "Update Berhasil";
            }
        }
        throw new BadRequestException();
    }

}