package id.ac.ukdw.thebooks_user.dto;

import lombok.Data;

@Data
public class UserDTO {
    
    private String email;

    private String namaUser;

    private String nomorTelepon;

    private String tanggalLahir;

    private byte[] fotoUser;
}