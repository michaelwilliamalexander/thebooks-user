package id.ac.ukdw.thebooks_user.dto.request;

import lombok.Data;

@Data
public class SignInRequest {
    
    private String email;

    public SignInRequest(){}

    public SignInRequest(String email){
        this.email = email;
    }
}
