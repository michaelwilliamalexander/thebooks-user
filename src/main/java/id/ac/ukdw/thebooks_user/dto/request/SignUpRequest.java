package id.ac.ukdw.thebooks_user.dto.request;

import lombok.Data;

@Data
public class SignUpRequest {
    
    private String email;
    
    private String namaUser;
    
    private String tanggalLahir;
    
    private String nomorTelepon;

    public SignUpRequest(){}

    public SignUpRequest(String email,
                         String namaUser,
                         String tanggalLahir,
                         String nomorTelepon){
        this.email = email;
        this.namaUser = namaUser;
        this.nomorTelepon = nomorTelepon;
        this.tanggalLahir = tanggalLahir;
    }
}
