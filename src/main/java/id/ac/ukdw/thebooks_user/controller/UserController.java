package id.ac.ukdw.thebooks_user.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.ac.ukdw.thebooks_user.dto.request.SignUpRequest;
import id.ac.ukdw.thebooks_user.dto.request.UpdateUserRequest;
import id.ac.ukdw.thebooks_user.dto.request.SignInRequest;
import id.ac.ukdw.thebooks_user.dto.response.ResponseWrapper;
import id.ac.ukdw.thebooks_user.exception.BadRequestException;
import id.ac.ukdw.thebooks_user.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.modelmapper.ModelMapper;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

import static id.ac.ukdw.thebooks_user.config.Swagger.userServiceTag;


@Controller
@RequestMapping("/auth")
@Api(tags = userServiceTag)
@RequiredArgsConstructor
public class UserController {

    private final ObjectMapper mapper;

    private final UserService service;

    @PostMapping(value = "/signup",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Membuat Akun Baru")
    public ResponseEntity<ResponseWrapper> signUp(@RequestBody SignUpRequest request){
        return ResponseEntity.ok(new ResponseWrapper(
                service.addUser(
                        request.getEmail(),
                        request.getNamaUser(),
                        request.getTanggalLahir(),
                        request.getNomorTelepon())));
    }

    @PostMapping(value = "/signin",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Login Akun")
    public ResponseEntity<ResponseWrapper> signIn(@RequestBody SignInRequest request){
        return ResponseEntity.ok(new ResponseWrapper(service.signIn(request.getEmail())));
    }

    @SneakyThrows
    @PostMapping(value = "/profile",consumes = {MediaType.MULTIPART_FORM_DATA_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE} )
    @ApiOperation(value = "Update data akun")
    public ResponseEntity<ResponseWrapper> update(
            @RequestPart("data") UpdateUserRequest request,
            @RequestPart("images") MultipartFile images){
        return ResponseEntity.ok(new ResponseWrapper(
            service.updateUser(
                    request.getEmail(),
                    request.getNamaUser(),
                    request.getNomorTelepon(),
                    request.getTanggalLahir(),
                    images.getBytes())
        ));
    }

    @GetMapping(value = "/profile",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Melihat Detail Akun")
    public ResponseEntity<ResponseWrapper> getDetailUserAccount(@RequestParam("email") String email){
        return ResponseEntity.ok(new ResponseWrapper(
                service.getDetailUser(email)));
    }
}
