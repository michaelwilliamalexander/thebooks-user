package id.ac.ukdw.thebooks_user.config;

import id.ac.ukdw.thebooks_user.service.UserService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class Swagger {

    public static final String userServiceTag = "User Service";

    @Bean
    public Docket authenticationApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("id.ac.ukdw.thebooks_user.controller"))
                .build()
                .apiInfo(metadata())
                .tags(new Tag(userServiceTag,"REST API mengelola data user"));
    }

    private ApiInfo metadata() {
        return new ApiInfoBuilder()
                .title("The Books")
                .description("Dokumentasi API User Service")
                .build();
    }
}
